var Sequelize = require('sequelize');
var sequelize = require('../server').sequelize;

var User = sequelize.define('user', {
  email: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  },
  rank: {
    type: Sequelize.DOUBLE
  },
  toLearn: {
    type: Sequelize.TEXT(1000)
  }
});

User.sync();

// User.sync({ force: true });

module.exports = User;