var Sequelize = require('sequelize');
var sequelize = require('../server').sequelize;

var Task = sequelize.define('task', {
  question: {
    type: Sequelize.STRING
  },
  answer1: {
    type: Sequelize.STRING
  },
  answer2: {
    type: Sequelize.STRING
  },
  answer3: {
    type: Sequelize.STRING
  },
  answer4: {
    type: Sequelize.STRING
  },
  correct: {
    type: Sequelize.STRING
  },
  coefficient: {
    type: Sequelize.DOUBLE
  },
  executionsСount: {
    type: Sequelize.INTEGER
  },
  correctCount: {
    type: Sequelize.INTEGER
  },
  additionalLinks: {
    type: Sequelize.TEXT
  }
});

Task.sync();

// Task.sync({
//   force: true
// });

module.exports = Task;