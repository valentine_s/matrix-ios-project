var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var crypto = require('crypto');

module.exports = function (passport) {
  var secret = '1234567';

  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (obj, done) {
    done(null, obj);
  });

  passport.use('local-signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  },

    function (req, email, password, done) {
      process.nextTick(function () {

        User.findOne({
          where: {
            email: email
          }
        })
          .then(function (user) {
            if (user) {
              return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            } else {
              var hash = crypto
                .createHmac('sha256', secret)
                .update(password)
                .digest('hex');
              console.log('hash ' + hash);
              User.create({
                email: email,
                password: hash,
                rank: undefined,
                toLearn: ''
              })
                .then(function (res) {
                  return done(null, res);
                });
            }
          });
      });
    }));

  passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  },
    function (req, email, password, done) {
      User.findOne({
        where: {
          email: email
        }
      })
        .then(function (user) {
          var hash = crypto
            .createHmac('sha256', secret)
            .update(password)
            .digest('hex');
          console.log('hash ' + hash);
          if (!user) {
            return done(null, false, req.flash('loginMessage', 'No user found.'));
          }
          if (user.password != hash) {
            return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
          }
          return done(null, user);
        });
    }));
};