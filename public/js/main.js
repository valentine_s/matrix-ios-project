'use strict'

window.onload = function () {
    var tts = new ya.speechkit.Tts({
        apikey: '952723e0-a7a9-4210-ae67-d4e7fe5e9e11',
        speaker: 'jane',
        emotion: 'good',
        gender: 'female'
    });

    $("span.speak").click(function (event) {
        var elem = this.cloneNode(true);
        for (var i = elem.childNodes.length - 1; i >= 0; i--) {
            if (elem.childNodes[i].tagName) {
                elem.removeChild(elem.childNodes[i]);
            }
        }
        var innerText = elem['innerText' in elem ? 'innerText' : 'textContent'];
        console.log(innerText);
        if (innerText != undefined) {
            tts.speak(innerText);
        }
        event.stopImmediatePropagation();
    });
};

function process() {
    var res = {};
    var arr = [];
    res.user = $('#user_id').val();

    $("form").each(function () {
        var obj = {};
        obj.id = this.id;

        $(this).children().each(function () {
            if ($(this).is(":checked")) {
                obj.answer = $(this).attr('value');
            }
        });
        arr.push(obj);
    });

    res.answers = arr;
    console.log(JSON.stringify(res));

    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/process', true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xhr.onload = function () {
        window.location.href = '/profile';
    };

    xhr.send(JSON.stringify(res));
};