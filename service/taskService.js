var sequelize = require('../server').sequelize;
var Sequelize = require('sequelize');
var Task = require('../models/task');

var gen = function generateTasks() {

    Task
        .create({
            question: 'Дайте определение матрицы размера M x N ?',
            answer1: 'Матрицей размера M x N называется таблица, образованная из элементов некоторого множества (например, чисел или функций) и имеющая M строк и N столбцов.',
            answer2: 'Матрицей размера M x N называется таблица, образованная из элементов некоторого множества (например, чисел или функций) и имеющая M*M строк и N*N столбцов."',
            answer3: 'Матрицей размера M x N называется список элементов размером M, где каждый элемент - число от 1 до N.',
            answer4: 'Матрицей размера M x N называется список элементов размером M, где каждый элемент - число от M до N.',
            correct: 'answer1',
            coefficient: 1,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Введение в матрицы'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Какую матрицу называют прямоугольной?',
            answer1: 'Матрицу, у которой число строк не равно числу столбцов.',
            answer2: 'Матрицу, у которой число строк равно числу столбцов.',
            answer3: 'Матрицу, у которой число строк больше числа столбцов.',
            answer4: 'Матрицу, у которой число строк меньше числа столбцов.',
            correct: 'answer1',
            coefficient: 1,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Введение в матрицы'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Диагональной называется матрица, у которой….',
            answer1: 'все элементы вне главной диагонали равны нулю',
            answer2: 'все элементы главной диагонали равны нулю',
            answer3: 'все элементы на главной и побочной диагоналях равны нулю',
            answer4: 'все элементы первой строки равны нулю',
            correct: 'answer1',
            coefficient: 1,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Введение в матрицы'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Скалярной матрицей называется матрица, у которой...',
            answer1: 'элементы, стоящие на главной диагонали, равны одному и тому же числу, отличному от нуля, а все прочие равны нулю',
            answer2: 'все элементы отличны от нуля',
            answer3: 'все элементы равны нулю',
            answer4: 'все диагональные элементы равны единице',
            correct: 'answer1',
            coefficient: 1,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Введение в матрицы'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Произведение матриц вычисляется следующим образом:',
            answer1: 'Каждый элемент каждой строки первой матрицы умножается на соответствующий по порядку элемент каждого столбца второй матрицы и их сумма записывается в элемент, первый индекс которого равен номеру строки первой матрицы, а второй индекс – номеру столбца второй матрицы.',
            answer2: 'Каждый элемент соответствующего столбца первой матрицы умножается на каждый элемент такой же по порядку строки второй матрицы и их произведение записывается в элемент соответствующего столбца матрицы-произведения.',
            answer3: 'Каждый элемент соответствующего столбца первой матрицы складывается с каждым элементом такого же по порядку столбца второй матрицы и их сумма записывается в элемент соответствующего столбца матрицы-произведения.',
            answer4: 'Каждый элемент соответствующего столбца первой матрицы умножается на каждый элемент такого же по порядку столбца второй матрицы и их произведение записывается в элемент соответствующего столбца матрицы-произведения.',
            correct: 'answer1',
            coefficient: 1,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Введение в матрицы'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Определитель произведения двух квадратных матриц равен…',
            answer1: 'произведению их определителей',
            answer2: 'сумме их определителей',
            answer3: 'нулю',
            answer4: 'единице',
            correct: 'answer1',
            coefficient: 1,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Введение в матрицы'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Обратная матрица для особой матрицы',
            answer1: 'не существует',
            answer2: 'существует и только одна',
            answer3: 'существует, причём несколько',
            answer4: 'существует и это транспонированная матрица',
            correct: 'answer1',
            coefficient: 1,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Введение в матрицы'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Чтобы вычислить произведение матрицы на число, нужно',
            answer1: 'умножить каждый элемент на число',
            answer2: 'умножить элементы первого столбца на число',
            answer3: 'умножить элементы первой строки на число',
            answer4: 'умножить элементы главной диагонали на число',
            correct: 'answer1',
            coefficient: 1,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Введение в матрицы'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Порядок может быть только у матрицы следующего вида:',
            answer1: 'квадратной',
            answer2: 'прямоугольной',
            answer3: 'любой',
            answer4: 'матрицы-строки',
            correct: 'answer1',
            coefficient: 1,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Введение в матрицы'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Две матрицы A и B считаются равными, если они…',
            answer1: 'одинакового размера, и элементы, стоящие в A и B на одинаковых местах, равны между собой, т.е. a ij = b ij.',
            answer2: 'одинакового размера, и элементы, стоящие в A и B на главной диагонали, равны между собой, т.е. a ij = b ij.',
            answer3: 'одинакового размера, и элементы, стоящие в A и B на главной диагонали, равны между собой, т.е. a ij = b ij.',
            answer4: 'одинакового размера, и элементы, стоящие в A и B на главной диагонали, равны между собой, т.е. a ij = b ij.',
            correct: 'answer1',
            coefficient: 1,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Введение в матрицы'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Определитель это:',
            answer1: 'Число',
            answer2: 'Матрица',
            answer3: 'Множество',
            answer4: 'Последовательность',
            correct: 'answer1',
            coefficient: 1,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Порядок определителя – это:',
            answer1: 'Число его строк и столбцов',
            answer2: 'Диапазон значений его элементов',
            answer3: 'Значение',
            answer4: 'Сумма индексов первого элемента первой строки',
            correct: 'answer1',
            coefficient: 1,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Правило треугольников это:',
            answer1: 'Правило вычисления определителя третьего порядка',
            answer2: 'Правило преобразования определителя',
            answer3: 'Правило вычисления определителя любого порядка',
            answer4: 'Правило образования миноров исходного определителя',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Минор определителя это:',
            answer1: 'Другой определитель',
            answer2: 'Сумма элементов главной диагонали',
            answer3: 'Произведение элементов главной диагонали',
            answer4: 'Значение определителя, взятое с обратным знаком',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Треугольный определитель равен:',
            answer1: 'Произведению элементов главной диагонали',
            answer2: 'нулю',
            answer3: 'единице',
            answer4: 'Разнице произведений элементов главной и побочной диагонали',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Если к элементам какой-либо строки или столбца прибавить произведение соответствующих элементов другой строки или столбца на постоянный множитель, то:',
            answer1: 'Значение определителя не изменится',
            answer2: 'Значение определителя будет умножено на постоянный множитель',
            answer3: 'Определитель будет преобразован в минор',
            answer4: 'Ни один из предыдущих ответов не верен',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Если в определителе поменять местами два соседних параллельных ряда (строки или столбцы), то значение определителя:',
            answer1: 'поменяет знак на противоположный',
            answer2: 'будет равен нулю',
            answer3: 'будет равен единице',
            answer4: 'не изменится',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Определитель какой матрицы не меняется при',
            answer1: 'квадратной',
            answer2: 'Прямоугольной',
            answer3: 'единичной',
            answer4: 'нулевой',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Что происходит с определителем  |A| матрицы при перестановке местами двух строк (столбцов) ?',
            answer1: 'определитель меняет знак',
            answer2: 'определитель будет равен 0',
            answer3: 'определитель будет равен 1',
            answer4: 'определитель будет равен 5',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Если все элементы ….. определителя |A| равны нулю, то и сам определитель равен нулю.',
            answer1: 'некоторой строки (столбца)',
            answer2: 'стоящие на главной диагонали',
            answer3: 'все элементы равны нулю',
            answer4: 'все элементы равны 3',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Какие параметры задают размер матрицы ?',
            answer1: 'количество строк и столбцов',
            answer2: 'определитель матрицы',
            answer3: 'количество строк и элементов',
            answer4: 'все элементы равны 3',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'При транспонировании матрицы ее определитель….',
            answer1: 'не меняется',
            answer2: 'равен 0',
            answer3: 'равен 1',
            answer4: 'меняет знак на противоположный',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Общий множитель элементов любой строки (столбца) можно…',
            answer1: 'выносить за знак определителя',
            answer2: 'не учитывать при вычислении определителя',
            answer3: 'не учитывать при вычислении определителя, но поменять знак определителя на противоположный',
            answer4: 'не учитывать при вычислении определителя, но умножить каждый элемент определителя на этот общий множитель',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'У какой матрицы есть определитель?',
            answer1: 'квадратной, размера 2х2',
            answer2: 'прямоугольной',
            answer3: 'трапециевидной',
            answer4: 'прямоугольной, размера 3х3',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'У какой матрицы есть  определитель?',
            answer1: 'квадратной',
            answer2: 'прямоугольной',
            answer3: 'нулевой',
            answer4: 'единичной',
            correct: 'answer1',
            coefficient: 2,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Главной диагональю в матрице называется….',
            answer1: 'Воображаемая прямая, соединяющая элементы определителя, у которых оба индекса одинаковы',
            answer2: 'Воображаемая прямая, соединяющая элементы определителя, у которых оба индекса разные',
            answer3: 'первый столбец в матрице',
            answer4: 'первая строка в матрице',
            correct: 'answer1',
            coefficient: 3,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Как называется диагональ, не являющаяся главной?',
            answer1: 'побочной',
            answer2: 'медианой',
            answer3: 'дополнительной',
            answer4: 'гипотенузой',
            correct: 'answer1',
            coefficient: 3,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Существует ли определитель у матрицы первого порядка (состоящей из 1 элемента)?',
            answer1: 'да, равен самому элементу',
            answer2: 'нет',
            answer3: 'да, равен 0',
            answer4: 'да, равен 1',
            correct: 'answer1',
            coefficient: 3,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Определитель'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Квадратной матрицей называется….',
            answer1: 'матрица, у которой количество строк равно количеству столбцов (размера n×n), число n называется порядком матрицы.',
            answer2: 'матрица, у которой количество строк не равно количеству столбцов, число строк называется порядком матрицы.',
            answer3: 'матрица, у которой количество строк равно количеству столбцов (размера n×n), число n^2 называется порядком матрицы.',
            answer4: 'матрица, у которой количество строк равно количеству столбцов (размера n×n), число n^2 называется порядком матрицы.',
            correct: 'answer1',
            coefficient: 3,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Виды матриц'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Нулевой матрицей называется матрица….',
            answer1: 'все элементы которой равны нулю, т.е. aij = 0, ∀i, j.',
            answer2: 'все элементы которой равны единице, т.е. aij = 1, ∀i, j.',
            answer3: 'все элементы которой не равны нулю, т.е. aij != 0, ∀i, j.',
            answer4: 'все элементы которой не равны нулю, т.е. aij != 0, ∀i, j.',
            correct: 'answer1',
            coefficient: 3,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Виды матриц'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Матрица, состоящая из одной строки называется:',
            answer1: 'вектором-строкой',
            answer2: 'вектором-столбцом',
            answer3: 'квадратной матрицей',
            answer4: 'диагональной матрицей',
            correct: 'answer1',
            coefficient: 3,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Виды матриц'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Матрица, состоящая из одного столбца называется:',
            answer1: 'вектором-столбцом',
            answer2: 'вектором-строкой',
            answer3: 'квадратной матрицей',
            answer4: 'диагональной матрицей',
            correct: 'answer1',
            coefficient: 3,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Виды матриц'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Диагональной матрицей называется….',
            answer1: 'квадратная матрица, все элементы которой, стоящие вне главной диагонали, равны нулю.',
            answer2: 'прямоугольная матрица, все элементы которой, стоящие вне главной диагонали, равны нулю.',
            answer3: 'прямоугольная матрица, все элементы которой, стоящие на главной диагонали, равны нулю.',
            answer4: 'квадратная матрица, все элементы которой, стоящие на главной диагонали, равны нулю.',
            correct: 'answer1',
            coefficient: 3,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Виды матриц'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Единичной матрицей называется…',
            answer1: 'диагональная матрица, диагональные элементы которой равны 1.',
            answer2: 'квадратная матрица, диагональные элементы которой равны 1.',
            answer3: 'квадратная матрица, все элементы которой равны 1.',
            answer4: 'диагональная матрица, диагональные элементы которой равны 0.',
            correct: 'answer1',
            coefficient: 3,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Виды матриц'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Что называют следом матрицы ?',
            answer1: 'Сумму главных диагональных элементов матрицы.',
            answer2: 'Сумму элементов матрицы, стоящих на побочной диагонали.',
            answer3: 'Сумму всех элементов матрицы.',
            answer4: 'Произведение всех элементов матрицы.',
            correct: 'answer1',
            coefficient: 3,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Виды матриц'
        })
        .then(function (res) {
            console.log('Added');
        });

    Task
        .create({
            question: 'Какую матрицу называют противоположной матрице А?',
            answer1: '-А, все элементы которой имеет противоположный знак.',
            answer2: '-А, элементы любой строки которой имеет противоположный знак.',
            answer3: '-А, элементы любого столбца которой имеет противоположный знак.',
            answer4: '-А, элементы которой являются обратными элементам матрицы А.',
            correct: 'answer1',
            coefficient: 3,
            executionsCount: 0,
            correctCount: 0,
            additionalLinks: 'Виды матриц'
        })
        .then(function (res) {
            console.log('Added');
        });
}

module.exports.gen = gen;