var task = require('../service/taskService').gen;
var Task = require('../models/task');
var sequelize = require('../server').sequelize;
var Sequelize = require('sequelize');
var User = require('../models/user');
var url = require('url');

module.exports = function (app, passport) {
    app.get('/', function (req, res) {
        if (req.isAuthenticated()) {
            res.redirect('/profile');
        } else {
            res.render('index.ejs');
        }
    });

    app.get('/demo', function (req, res) {
        res.send(200);
    });

    app.get('/login', function (req, res) {
        res.render('login.ejs', {
            message: req.flash('loginMessage')
        });
    });

    app.get('/signup', function (req, res) {
        res.render('signup.ejs', {
            message: req.flash('signupMessage')
        });
    });

    app.get('/profile', isLoggedIn, function (req, res) {
        var localUser = req.user;

        if (!localUser.rank) {
            var url_parts = url.parse(req.url, true);
            var query = url_parts.query;
            var defaultRank = req.query.defaultRank;
            console.log("DefaultRank: " + defaultRank);
            localUser.rank = defaultRank;

            User.findOne({
                where: {
                    email: localUser.email
                }
            })
                .then(function (userFromDb) {
                    if (userFromDb.rank) {
                        localUser.rank = userFromDb.rank;
                    } else {
                        userFromDb.updateAttributes({
                            rank: localUser.rank
                        })
                            .success(function () {
                            })
                    }
                })
        }

        res.render('profile.ejs', {
            user: localUser
        });
    });

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/knowledge',
        failureRedirect: '/signup',
        failureFlash: true
    }));

    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/profile',
        failureRedirect: '/login',
        failureFlash: true
    }));

    app.get('/first_test', function (req, res) {
        if (!req.isAuthenticated()) {
            res.redirect('/');
        } else {
            Task
                .findAll({
                    limit: 3,
                    order: [
                        Sequelize.fn('RANDOM'),
                    ]
                })
                .then(function (tasks) {
                    var data = tasks.map(function (item) {
                        return item.dataValues;
                    });
                    console.log(req.body);
                    res.render('test', {
                        tasks: data,
                        user: req.user
                    });
                });
        }
    });

    // gen tasks in db
    app.get('/gen', function (req, res) {
        task();
        res.send(200);
    });

    app.post('/process', function (req, res) {
        var rank;
        var isStart = false;
        var toStudy;

        var user = User.findOne({
            where: {
                id: req.body.user
            }
        }).then(function (user) {
            console.log('rank ' + user.rank);
            rank = user.rank;

            if (isNaN(rank)) {
                console.log('rank is NaN');
                rank = 0;
                isStart = true;
                toStudy = [];
            } else {
                toStudy = user.toLearn.split(' | ');
            }

            var s = new Set(toStudy);
            console.log('rank ' + rank);

            req.body.answers.forEach(function (el) {
                Task
                    .findOne({
                        where: {
                            id: el.id
                        }
                    })
                    .then(function (task) {
                        var correctAnswer = task.dataValues.correct;
                        var userAnswer = el.answer;
                        var correctCount = task.correctCount;
                        if (correctAnswer === userAnswer) {
                            s.delete(task.additionalLinks);
                            console.log('correctAnswer');
                            if (isStart) {
                                console.log('Start');
                                rank++;
                            } else {
                                rank = (rank + task.coefficient / 10);
                            }
                            correctCount = correctCount + 1;
                        } else {
                            console.log('incorrectAnswer');
                            if (!isStart) {
                                rank = (rank - task.coefficient / 10);
                            }
                            s.add(task.dataValues.additionalLinks);
                        }
                        var executionsСount = task.executionsСount + 1;
                        task
                            .updateAttributes({
                                correctCount: correctCount,
                                executionsСount: executionsСount
                            })
                            .then(function () {
                                if (rank < 0) {
                                    rank = 0;
                                }
                                console.log('final rank ' + rank);
                                user
                                    .updateAttributes({
                                        rank: rank.toFixed(2),
                                        toLearn: Array.from(s).join(' | ')
                                    })
                                    .then(function (user) {
                                        req.logIn(user, function (error) {
                                            console.log('relogin');
                                        });
                                        res.send(200);
                                    });
                            });
                    });
            });
        });
    });

    app.get('/test', function (req, res) {
        if (!req.isAuthenticated()) {
            res.redirect('/');
        } else {
            rank = req.user.rank;
            if (rank <= 1) {
                rank = 1;
            }
            Task
                .findAll({
                    limit: 3,
                    where: {
                        coefficient: {
                            $gte: rank
                        }
                    },
                    order: [
                        Sequelize.fn('RANDOM'),
                    ]
                })
                .then(function (tasks) {
                    var data = tasks.map(function (item) {
                        return item.dataValues;
                    });
                    res.render('test', {
                        tasks: data,
                        user: req.user
                    });
                });
        }
    });

    app.get('/update', function (req, res) {
        Task
            .findAll()
            .then(function (tasks) {
                for (var i = 0; i < tasks.length; i++) {
                    var task = tasks[i];

                    var executionsСount = task.executionsСount;
                    var correctCount = task.correctCount;

                    var rank;

                    if (correctCount === 0 && executionsСount != 0) {
                        rank = 3;
                    } else if (executionsСount === 0) {
                        rank = 1;
                    } else {
                        rank = correctCount / executionsСount;

                        if (rank > 0.6) {
                            rank = 1;
                        } else if (rank < 0.3) {
                            rank = 3;
                        } else {
                            rank = 2;
                        }
                    }

                    task
                        .updateAttributes({
                            coefficient: rank
                        });
                }
            });
        res.send(200);
    });

    app.get('/theory', function (req, res) {
        if (req.isAuthenticated()) {
            res.render('theory.ejs', {
                user: req.user
            });
        } else {
            res.render('index.ejs');
        }
    });

    app.get('/knowledge', function (req, res) {
        if (req.isAuthenticated()) {
            res.render('knowledge_level.ejs', {
                user: req.user
            });
        } else {
            res.render('index.ejs');
        }
    });

    app.get('/all', function (req, res) {
        Task
            .findAll()
            .then(function (tasks) {
                var data = tasks.map(function (item) {
                    return item.dataValues;
                });
                res.json(data);
            });
    });
};

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
};