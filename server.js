var express = require('express');

var passport = require('passport');
var Sequelize = require('sequelize');
var flash = require('connect-flash');
var pg = require('pg');
var path = require('path');

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var configDB = require('./config/database.js');

var port = process.env.PORT || 8080;

var app = express();

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser());

app.set('view engine', 'ejs');

app.use(session({
  secret: 'helloworld'
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));

var sequelize = new Sequelize(configDB.database, configDB.username, configDB.password, {
  host: configDB.host,
  dialect: configDB.dialect,

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },

  dialectOptions: {
    ssl: false
  }
});

sequelize
  .authenticate()
  .then(function (err) {
    console.log('Connection has been established successfully.');
  })
  .catch(function (err) {
    console.log('Unable to connect to the database:', err);
  });

exports.sequelize = sequelize;

require('./routes/routes.js')(app, passport);
require('./config/passport.js')(passport);

app.listen(port);
console.log('The magic happens on port ' + port);